package finde.api;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class GetData {



    @SneakyThrows
    public void getUsersByCount() {

        Map<String, List<NamePossition.NamePos>> result = new HashMap<>();

        URL url = new URL("http://norvig.com/big.txt");
        BufferedReader read = new BufferedReader(
                new InputStreamReader(url.openStream()));

        int rowNum = 1;
        String i;
        while ((i = read.readLine()) != null) {
            Map<String, List<Integer>> allNamesInSingleString = findAllNamesInSingleString(i);

            for(Map.Entry<String, List<Integer>> pair : allNamesInSingleString.entrySet()) {
                String name = pair.getKey();
                List<Integer> pos = pair.getValue();

                List<NamePossition.NamePos> allNamePos = result.get(name);

                int row = rowNum;
                List<NamePossition.NamePos> namePos = pos.stream()
                        .map(p -> new NamePossition.NamePos(row, p))
                        .collect(toList());
                if (allNamePos == null) {
                    result.put(name, namePos);
                } else {
                    allNamePos.addAll(namePos);
                }
            }

            rowNum++;
        }
        read.close();

        result.forEach((k, v) -> {
            System.out.printf("%s -> [%s]\n", k, v.stream().map(NamePossition.NamePos::toString).collect(joining(", ")));
        });
    }


    private Map<String, List<Integer>> findAllNamesInSingleString(String string) {

        Map<String, List<Integer>> namesInSingleString = new HashMap<>();

        DataNames.names.forEach(n -> {
            List<Integer> namePositionsInString = nameCountInString(new ArrayList<>(), 0, n, string);
            if (!namePositionsInString.isEmpty()) {
                namesInSingleString.put(n, namePositionsInString);
            }
        });

        return namesInSingleString;
    }

    private List<Integer> nameCountInString(List<Integer> namePositions, int offset, String name, String string) {

        int none = -1;

        if (offset == none) {
            return namePositions;
        }

        int pos = string.indexOf(name, ++offset);

        if (pos != none) {
            namePositions.add(pos);
        }

        return nameCountInString(namePositions, pos, name, string);
    }


}
