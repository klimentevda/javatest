package finde.api;

import java.util.List;

import static java.util.Arrays.asList;

public class DataNames {

    public static final List<String> names = asList(
            "Richard",
            "Charles",
            "James",
            "John",
            "Robert",
            "Michael",
            "William",
            "David",
            "Joseph",
            "Thomas",
            "Christopher",
            "Daniel",
            "Paul",
            "Mark",
            "Donald",
            "George",
            "Kenneth",
            "Steven",
            "Edward",
            "Brian",
            "Ronald",
            "Anthony",
            "Kevin",
            "Jason",
            "Matthew",
            "Gary",
            "Timothy",
            "Jose",
            "Larry",
            "Jeffrey",
            "Frank",
            "Scott",
            "Eric",
            "Stephen",
            "Andrew",
            "Raymond",
            "Gregory",
            "Joshua",
            "Jerry",
            "Dennis",
            "Walter",
            "Patrick",
            "Peter",
            "Har old",
            "Douglas",
            "Henry",
            "Carl",
            "Arthur",
            "Ryan",
            "Roger"
    );
}
