package finde.api;

import lombok.RequiredArgsConstructor;

public class NamePossition {

    @RequiredArgsConstructor
    public static class NamePos {
        private final int row;
        private final int position;

        @Override
        public String toString() {
            return String.format("[lineOffset=%s, charOffset=%s]", row, position);
        }
    }

}
