package generation;

import generation.logic.WorkWithData;

import java.util.Scanner;

public class Gen extends Basic {

    public static void main(String[] args) {
        scan = new Scanner(System.in);
        work = new WorkWithData();

        generate();

        System.out.println("users created.");
        Basic.menu();

        System.out.print("next step:");
        number = scan.nextInt();
        work.work(number, users);

        while (!(number == 5)) {
            System.out.print("next step:");
            number = scan.nextInt();
            work.work(number, users);
        }

        work.work(number, users);
    }
}
