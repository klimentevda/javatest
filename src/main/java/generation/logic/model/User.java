package generation.logic.model;

import lombok.Data;

@Data
public class User {
    String name;
    String surname;
    Gender gender;
    String region;
}
