package generation.logic.user;

import generation.logic.core.GetIdList;
import generation.logic.model.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Users {
    ArrayList<UserBuilder> users;
    HashSet idList;

    private GetIdList id = new GetIdList();

    public ArrayList<UserBuilder> getUsersList(int count) {

        users = new ArrayList<>();
        idList = id.randomIndividualId(count);

        for (Object anIdList : idList) {

            users.add(UserBuilder.builder()
                    .name("Name_" + anIdList.toString())
                    .lastName("LastName_" + anIdList.toString())
                    .emal("Email_" + anIdList.toString() + "@generation.com")
                    .country("County_test")
                    .city("City_test")
                    .idUser(anIdList.toString())
                    .build());
        }

        return users;
    }

    public ArrayList<UserBuilder> getUsersListFromApi(List<User> usersApi) {

        users = new ArrayList<>();
        idList = id.randomIndividualId(usersApi.size());
        ArrayList list = new ArrayList();

        for (Object i : idList)
            list.add(i);

        for (int i = 0; i< usersApi.size(); i++) {
            users.add(UserBuilder.builder()
                    .name(usersApi.get(i).getName())
                    .lastName(usersApi.get(i).getSurname())
                    .emal(usersApi.get(i).getName() + usersApi.get(i).getGender() + "@generation.com")
                    .country(usersApi.get(i).getRegion() + "country_test")
                    .city(usersApi.get(i).getRegion())
                    .idUser(String.valueOf(list.get(i)))
                    .build());
        }
        return users;
    }

}
