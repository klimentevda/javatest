package generation.logic.api;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import lombok.SneakyThrows;
import generation.logic.model.User;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

public class GetData {


    @SneakyThrows
    public User[] getUsersByCount(int count) {
        RestAssured.baseURI = "https://uinames.com/api";

        return given()
                .queryParam("amount", count)
                .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
                .when().get().as(User[].class);
    }

    public List<User> getUsersListByCount(int count){
    return Arrays.asList(getUsersByCount(count));

    }
}
