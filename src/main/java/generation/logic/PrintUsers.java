package generation.logic;
import generation.logic.user.UserBuilder;

import java.util.ArrayList;

public class PrintUsers {

    public static void printAllUsers(ArrayList<UserBuilder> users) {
        users.forEach(a -> printUser(a));
    }

    public static void printUser(UserBuilder object) {
        System.out.println(
                object.getEmal() + ",  " +
                        object.getName() + ",  " +
                        object.getLastName() + ",  " +
                        object.getCountry() + ",  " +
                        object.getCity() + ",  " +
                        object.getIdUser());
    }
}
