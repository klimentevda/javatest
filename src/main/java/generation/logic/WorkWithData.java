package generation.logic;


import jxl.write.WriteException;
import generation.Basic;
import generation.logic.api.GetData;
import generation.logic.export.ExcelFile;
import generation.logic.export.TxtFile;
import generation.logic.model.User;
import generation.logic.user.UserBuilder;
import generation.logic.user.Users;

import java.util.ArrayList;
import java.util.List;

public class WorkWithData extends Basic {

    public void work(int step, ArrayList<UserBuilder> users) {
        switch (step) {
            case 1:
                generate();
                System.out.println("users created.");
                Basic.menu();
                break;
            case 2:
                PrintUsers.printAllUsers(users);
                System.out.println("Print users - done");
                break;
            case 3:
                try {
                    String fileName = new ExcelFile().createFile(users);
                    System.out.println("export users in xml - done: Name file = " + fileName);
                } catch (WriteException e) {
                    e.printStackTrace();
                }
                break;
            case 4:
                String fileName = new TxtFile().createFile(users);
                System.out.println("export users in txt - done: Name file = " + fileName);

                break;
            case 5:
                scan.close();
                break;
            default:
                throw new IllegalArgumentException("not valid step");
        }
    }

    public ArrayList<UserBuilder> methods(int step, int number) {
        switch (step) {
            case 1:
                return new Users().getUsersList(number);
            case 2:
                List<User> randomUsers = new GetData().getUsersListByCount(number);
                return new Users().getUsersListFromApi(randomUsers);
            default:
                throw new IllegalArgumentException("not valid step");
        }
    }


}
