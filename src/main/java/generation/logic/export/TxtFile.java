package generation.logic.export;

import generation.logic.user.UserBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class TxtFile extends File {

    private FileWriter writer;

    public String createFile(ArrayList<UserBuilder> users) {
        String fullString =fileName + ".txt";
        CreateFile(fullString);
        setValue(users, fullString);
        return fullString;
    }


    private void setValue(ArrayList<UserBuilder> users, String puth) {
        try {
            writer = new FileWriter(puth);
        } catch (IOException e) {
            e.printStackTrace();
        }

        users.forEach(a -> setUser(a, puth));
        try {
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setUser(UserBuilder object, String puth) {
        try {
            writer.write(
                    object.getEmal() + ", " +
                            object.getName() + ", " +
                            object.getLastName() + ", " +
                            object.getCountry() + ", " +
                            object.getCountry() + ", " +
                            object.getCity() + ", " +
                            object.getIdUser() + '\n');
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

