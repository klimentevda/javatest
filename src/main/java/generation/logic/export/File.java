package generation.logic.export;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import java.io.IOException;
import java.util.Random;

public class File {
    private int random = new Random().nextInt(99999);
    protected String path = new java.io.File("").getAbsolutePath();
    protected String fileName = path + "\\"+ "users_list_test_" + random;
    protected WritableWorkbook workbook;
    protected static WritableSheet sheet;

    protected void CreateFile(String fileName) {
        try {
            workbook = Workbook.createWorkbook(new java.io.File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void writeFile() {
        try {
            workbook.write();
            workbook.close();
        } catch (IOException | WriteException e) {
            throw new RuntimeException(e);
        }
    }


    protected WritableWorkbook getWorkbook() {
        return workbook;
    }
}
