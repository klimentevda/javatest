package generation.logic.export;

import jxl.write.Label;
import jxl.write.WriteException;
import generation.logic.user.UserBuilder;

import java.util.ArrayList;
import java.util.List;

public class ExcelFile extends File {

    private List<String> email = new ArrayList<>();
    private List<String> name = new ArrayList<>();
    private List<String> lastName = new ArrayList<>();
    private List<String> country = new ArrayList<>();
    private List<String> city = new ArrayList<>();
    private List<String> personId = new ArrayList<>();

    public String createFile(ArrayList<UserBuilder> users) throws WriteException {
        String fullString = fileName + ".xls";
        CreateFile(fullString);
        sheet = getWorkbook().createSheet("TestCases", 0);
        createBodyExcelCall();
        setValue(users);
        writeFile();
        return fullString;
    }

    private static void createBodyExcelCall() throws WriteException {

        Label emailCall = new Label(0, 0, "email");
        Label nameCall = new Label(1, 0, "name");
        Label lastNameCall = new Label(2, 0, "lastName");
        Label countryCall = new Label(3, 0, "country");
        Label cityCall = new Label(4, 0, "city");
        Label personIdCall = new Label(5, 0, "personId");

        //add to the cells
        sheet.addCell(emailCall);
        sheet.addCell(nameCall);
        sheet.addCell(lastNameCall);
        sheet.addCell(countryCall);
        sheet.addCell(cityCall);
        sheet.addCell(personIdCall);
    }

    private void setValue(ArrayList<UserBuilder> excelData) throws WriteException {

        excelData.forEach(a -> addInLists(a));

        type(email, 0);
        type(name, 1);
        type(lastName, 2);
        type(country, 3);
        type(city, 4);
        type(personId, 5);

    }

    private void addInLists(UserBuilder object){
        email.add(object.getEmal());
        name.add(object.getName());
        lastName.add(object.getLastName());
        country.add(object.getCountry());
        city.add(object.getCity());
        personId.add(object.getIdUser());
    }


    private void type(List<String> data, int call) {

        int index;

        for (index = 0; index != data.size(); index++) {
            Label Comment = new Label(call, index + 1, data.get(index));

            try {
                sheet.addCell(Comment);
            } catch (WriteException e) {
                e.printStackTrace();
            }

        }
    }
}
