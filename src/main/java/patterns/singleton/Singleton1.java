package patterns.singleton;


public class Singleton1 {
    private static Singleton1 instance;

    /**
     Область применения
     1.) В системе должно существовать не более одного экземпляра заданного класса.
     2.) Экземпляр должен быть легко доступен для всех клиентов данного класса.
     3.) Создание объекта on demand, то есть, когда он понадобится первый раз, а не во время инициализации системы.
     */


    private Singleton1() {
        Key.print();
    }

    public static Singleton1 getInstance() {
        if (instance == null) {
            instance = new Singleton1();
        }
        return instance;
    }

    /**
     У этого решения есть единственный недостаток –
     оно не работает в многопоточной среде и поэтому
     не подходит в большинстве случаев.
     */

}



