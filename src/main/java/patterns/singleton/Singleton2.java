package patterns.singleton;


public class Singleton2 {

    private static Singleton2 instance =  new Singleton2();

    private Singleton2() {
        Key.print();
    }

    public static Singleton2 getInstance() {
        return instance;
    }

    /**
     И вы будете правы, так как проблему многопоточности мы решили, но потеряли две важные вещи:
     1. Ленивую инициализацию (Объект instance будет создан classloader-ом во время инициализации класса)
     2. Отсутствует возможность обработки исключительных ситуаций(exceptions) во время вызова конструктора.
     */

}



