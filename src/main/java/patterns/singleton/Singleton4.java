package patterns.singleton;


public class Singleton4 {
    /** Била Пью(Bill Pugh) “Initialization on Demand Holder” */

    private static class SingletonHolder {
       private static final Singleton4 instance =  new Singleton4();
    }

    private Singleton4() {
        Key.print();
    }

    public static Singleton4 getInstance() {
        return SingletonHolder.instance;
    }

    /**
     В данном случае мы полностью решили проблему ленивой инициализации
     – объект инициализируется при первом вызове метода getInstance().
     Но у нас осталась проблема с обработкой исключительных ситуаций в конструкторе.
     Так что, если конструктор класса не вызывает опасений создания исключительных ситуаций,
     то смело можно использовать этот метод..
     */

}



