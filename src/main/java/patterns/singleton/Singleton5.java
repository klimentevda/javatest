package patterns.singleton;


public class Singleton5 {
    private static Singleton5 instance;

    /**
     Синхронизация
     */


    private Singleton5() {
        Key.print();
    }

    public static synchronized Singleton5 getInstance() {
        if (instance == null) {
            instance = new Singleton5();
        }
        return instance;
    }

    /**
     Синхронизация полезна только один раз, при первом обращении к getInstance(),
     после этого каждый раз, при обращении этому методу, синхронизация просто забирает время.
     */

}



