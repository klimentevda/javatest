package patterns.singleton;


public class Singleton3 {
    /** Била Пью(Bill Pugh) “Initialization on Demand Holder” */

    private static class SingletonHolder {
       private static final Singleton3 instance =  new Singleton3();
    }

    private Singleton3() {
        Key.print();
    }

    public static Singleton3 getInstance() {
        return SingletonHolder.instance;
    }

    /**
     И вы будете правы, так как проблему многопоточности мы решили, но потеряли две важные вещи:
     1. Ленивую инициализацию (Объект instance будет создан classloader-ом во время инициализации класса)
     2. Отсутствует возможность обработки исключительных ситуаций(exceptions) во время вызова конструктора.
     */

}



