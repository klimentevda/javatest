package patterns.singleton;


public enum Singleton9Holder {
    INSTANCE(new Singleton9());
    private final Singleton9 singleton;

    Singleton9Holder(Singleton9 singleton) {
        this.singleton = singleton;
    }
    public Singleton9 getSingleton(){
        return singleton;
    }
    public static class Singleton9 {
        void print() {
            Key.print();
        }
    }

}



