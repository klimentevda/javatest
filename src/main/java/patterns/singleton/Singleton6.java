package patterns.singleton;


public class Singleton6 {

    private static volatile Singleton6 instance;

    /**
     Double-Checked Locking
     */


    private Singleton6() {
        Key.print();
    }

    public static  Singleton6 getInstance() {
        synchronized (Singleton6.class){
        if (instance == null) {
            instance = new Singleton6();
        }}
        return instance;
    }

    /**
     Не смотря на то, что этот вариант выглядит как идеальное решение,
     использовать его не рекомендуется т.к. товарищ Allen Holub заметил,
     что использование volatile модификатора может привести к проблемам производительности
     на мультипроцессорных системах. Но решать все же вам.
     */

}



