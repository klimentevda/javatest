package patterns.builders;

public class FilterUserBuilder {

    private  String nameFilterInput = "";
    private  String lastNameFilterSelect = "";

    private  Boolean onlyMerredCheckBox = null;


    /** реализация */
    public void build() {
        if (!nameFilterInput.isEmpty()) {
            System.out.println("select(Button.nameFilterInput = )" + nameFilterInput);
        }

        if (!lastNameFilterSelect.isEmpty()) {
            System.out.println("type(Button.LastNameFilterInput  = )" + lastNameFilterSelect);
        }

        if (!(onlyMerredCheckBox == null)) {
            System.out.println("click(Button.LastNameFilterInput checkBox  = )" + lastNameFilterSelect);
        }

        System.out.println("click(Button.find)");
    }



    /** Сетеры */
    public FilterUserBuilder setNameFilterInput(String nameFilterInput) {
        this.nameFilterInput = nameFilterInput;
        return this;
    }

    public FilterUserBuilder setLastNameFilterInput(String lastNameFilterInput) {
        this.lastNameFilterSelect = lastNameFilterInput;
        return this;
    }

    public FilterUserBuilder setOnlyMerredCheckBox(Boolean onlyMerredCheckBox) {
        this.onlyMerredCheckBox = onlyMerredCheckBox;
        return this;
    }
}
