package patterns.builders;

import lombok.Builder;





public class FilterUserBuilderLombok {

    private final String userName;
    private final String userLastName ;
    private final Boolean onlyMarriedCheckBox;


    @Builder
    private FilterUserBuilderLombok(String userName, String userLastName,Boolean onlyMarriedCheckBox) {
        this.userName = userName;
        this.userLastName = userLastName;
        this.onlyMarriedCheckBox = onlyMarriedCheckBox;
    }



    /** реализация */
    public void useFilter() {
        if (!(userName == null)) {
            System.out.println("select(Button.nameFilterInput = )" + userName);
        }

        if (!(userLastName == null)) {
            System.out.println("type(Button.LastNameFilterInput  = )" + userLastName);
        }

        if (!(onlyMarriedCheckBox == null)) {
            System.out.println("click(Button.LastNameFilterInput checkBox  = )" + onlyMarriedCheckBox);
        }

        System.out.println("click(Button.find)\n");
    }

}
