package patterns.builders;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UserBuilderLombok {

    private final String emal;
    private final String name;
    private final String lastName;
    private final String city;
    private final String country;
    private final String idUser;

    @Builder
    private UserBuilderLombok(String emal, String lastName, String city, String country, String name, String idUser) {
        this.idUser = idUser;
        this.emal = emal;
        this.lastName = lastName;
        this.city = city;
        this.country = country;
        this.name = name;
    }
}
