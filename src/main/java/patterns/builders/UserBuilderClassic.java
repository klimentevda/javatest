package patterns.builders;

public class UserBuilderClassic {

    private  String name;
    private  String lastName;
    private  Boolean rich;


    public static Builder newBuilder() {
        return new Builder();
    }


    /**Конструктор*/
    public static  class Builder {
        private UserBuilderClassic newUser = new UserBuilderClassic();

        /**Параметры*/
        public Builder setName(String name) {
            newUser.name = name;
            return this;
        }

        public Builder setLastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }

        public Builder setRich(Boolean rich) {
            newUser.rich = rich;
            return this;
        }

        /**Метод который возвращает готовый обьект*/
        public UserBuilderClassic build() {
            return newUser;
        }
    }

    /**Параметры вывода*/
    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Boolean getRich() {
        return rich;
    }
}
