package patterns.builders;

import org.testng.annotations.Test;

public class TestBuilders {

    @Test
    public void userBuilderClassicTest(){
        UserBuilderClassic user1  = new UserBuilderClassic.Builder()
                .setName("Петька")
                .setLastName("Петькович")
                .setRich(true)
                .build();


        UserBuilderClassic user2  = new UserBuilderClassic.Builder()
                .setName("Витька")
                .build();


        System.out.println(user1.getLastName());
        System.out.println(user1.getName());
        System.out.println(user1.getRich());


        System.out.println(user2.getLastName());
        System.out.println(user2.getName());
        System.out.println(user2.getRich());
    }


    @Test
    public void userBuilderLombokUserTest(){
        UserBuilderLombok user1 = UserBuilderLombok.builder()
                .name("Витька")
                .lastName("Витькович")
                .build();


        UserBuilderLombok user2 = UserBuilderLombok.builder()
                .lastName("Петькович")
                .build();

        System.out.println(user1.getLastName());
        System.out.println(user1.getName());

        System.out.println(user2.getLastName());
        System.out.println(user2.getName());
    }


    @Test
    public void filterUserBuilderLombokTest(){
        new FilterUserBuilder()
                .setLastNameFilterInput("Ищем Петьку")
                .setOnlyMerredCheckBox(true)
                .build();
    }

    @Test
    public void filterUserBuilderTest(){
        FilterUserBuilderLombok filter1 = FilterUserBuilderLombok.builder()
                .userName("юзер1")
                .userLastName("юзер1")
                .onlyMarriedCheckBox(true)
                .build();

        FilterUserBuilderLombok filter2 = FilterUserBuilderLombok.builder()
                .userName("юзер2")
                .build();

        filter1.useFilter();
        filter2.useFilter();
    }


    /**
     *
     * код выглядит гораздо читабельнее. Ведь все, относящееся к созданию объекта, вынесено в отдельный класс  - Builder;
     * при заполнении полей объекта теперь параметры трудно перепутать;
     * мы можем заполнять не все параметры класса. Как Вы могли заметить, мы указали все параметры кроме родителей (parents).
     *
     * */


}
