package patterns.singleton;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestSingleton {

    private  long time;
    private static final int POOL_SIZE = 2000;
    private static final int COUNT = 2000;
    private static final boolean SINGLE = false;

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton1(){
        Singleton1 firstInstance = Singleton1.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton1 secondInstance = Singleton1.getInstance();

        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );
    }

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton2(){
        Singleton2 firstInstance = Singleton2.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton2 secondInstance = Singleton2.getInstance();

        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );
    }

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton3(){
        Singleton3 firstInstance = Singleton3.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton3 secondInstance = Singleton3.getInstance();

        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );
    }

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton4(){
        Singleton4 firstInstance = Singleton4.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton4 secondInstance = Singleton4.getInstance();

        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );
    }

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton5(){
        Singleton5 firstInstance = Singleton5.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton5 secondInstance = Singleton5.getInstance();


        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );
    }

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton6(){
        Singleton6 firstInstance = Singleton6.getInstance();

        System.out.println(firstInstance.getClass());

        Singleton6 secondInstance = Singleton6.getInstance();

        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );
    }

    @Test(threadPoolSize  = POOL_SIZE, invocationCount = COUNT,singleThreaded = SINGLE)
    public void testSingleton9(){
        Singleton9Holder.Singleton9 firstInstance = Singleton9Holder.INSTANCE.getSingleton();
        Singleton9Holder.Singleton9 secondInstance = Singleton9Holder.INSTANCE.getSingleton();

        firstInstance.print();

        Assert.assertTrue(firstInstance == secondInstance, "" +
                firstInstance +  "   |   " +  firstInstance );

    }



    @BeforeClass
    public void start(){
        time = System.nanoTime();
        System.out.printf("Start\n");

    }

    @AfterClass
    public void finish(){
        time = System.nanoTime() - time;
        System.out.printf("Elapsed %,9.3f ms\n", time/1_000_000.0);
    }

}
